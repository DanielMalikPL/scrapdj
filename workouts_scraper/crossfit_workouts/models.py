from django.db import models
from dynamic_scraper.models import Scraper, SchedulerRuntime
from scrapy_djangoitem import DjangoItem

class Website(models.Model):
    title = models.CharField(max_length=128)
    url = models.URLField()
    scraper = models.ForeignKey(Scraper, blank=True, null=True, on_delete=models.SET_NULL)
    scraper_runtime = models.ForeignKey(SchedulerRuntime, blank=True, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.title

class Workout(models.Model):
    title = models.CharField(max_length=128)
    # date = models.DateField(auto_now=False, auto_now_add=False)
    description = models.TextField()
    url = models.URLField()
    seed = models.ForeignKey(Website)

    def __unicode__(self):
        return self.title

class WorkoutItem(DjangoItem):
    django_model = Workout
