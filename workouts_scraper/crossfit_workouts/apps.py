from django.apps import AppConfig


class CrossfitWorkoutsConfig(AppConfig):
    name = 'crossfit_workouts'
